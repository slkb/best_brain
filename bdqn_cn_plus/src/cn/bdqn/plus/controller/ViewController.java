package cn.bdqn.plus.controller;
 
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.tb49.a.pojo.StudentExam;
import com.tb49.a.service.ViewService;

@SuppressWarnings("serial")
public class ViewController extends HttpServlet {
	
	private ViewService service = new ViewService();
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json");
		String date = request.getParameter("date");
		List<StudentExam> list = service.findOneDayStudentTopN(date, 20);
		PrintWriter pw = response.getWriter();
		pw.print(JSON.toJSONString(list));
		pw.close();
	}
}
