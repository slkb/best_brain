
function createRandomItemStyle() {
	return {
		normal: {
			color: 'rgb(' + [
				Math.round(Math.random() * 160),
				Math.round(Math.random() * 160),
				Math.round(Math.random() * 160)
			].join(',') + ')'
		}
	};
}
var myChart = echarts.init(document.getElementById('main'));
myChart.setOption({
	tooltip: {
		trigger: 'axis'
	},
	legend: {
		data: ['做题量', '正确率']
	},
	toolbox: {
		show: true,
		feature: {
			// mark : {show: true},
			// dataView : {show: true, readOnly: false},
			// magicType : {show: true, type: ['line', 'bar']},
			// restore : {show: true},
			saveAsImage: { show: true }
		}
	},
	calculable: true,
	xAxis: [{
		type: 'category',
		data: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月']
	}],
	yAxis: [{
		type: 'value',
		splitArea: { show: true }
	}],
	series: [{
			name: '做题量',
			type: 'bar',
			data: [2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3]
		},
		{
			name: '正确率',
			type: 'bar',
			data: [2.6, 5.9, 9.0, 26.4, 28.7, 70.7, 175.6, 182.2, 48.7, 18.8, 6.0, 2.3]
		}
	]
});

var nameCloud = echarts.init(document.getElementById('namecloud'));
nameCloud.setOption({
	title: {
		text: '本周中博风云人物榜',
		x: 'center',
		link: 'http://www.mengfanding.cn'
	},
	tooltip: {
		show: true
	},
	series: [{
		name: '本周风云人物榜',
		type: 'wordCloud',
		size: ['80%', '80%'],
		textRotation: [0, 45, 90, -45],
		textPadding: 0,
		autoSize: {
			enable: true,
			minSize: 14
		},
		data: [{
				name: "章伟",
				value: 10000,
				itemStyle: {
					normal: {
						color: 'black'
					}
				}
			},
			{
				name: "陆清泉",
				value: 6181,
				itemStyle: createRandomItemStyle()
			},
			{
				name: "王俊",
				value: 4386,
				itemStyle: createRandomItemStyle()
			},
			{
				name: "鲁国祥",
				value: 4055,
				itemStyle: createRandomItemStyle()
			},
			{
				name: "Charter Communications",
				value: 2467,
				itemStyle: createRandomItemStyle()
			},
			{
				name: "Chick Fil A",
				value: 2244,
				itemStyle: createRandomItemStyle()
			},
			{
				name: "Planet Fitness",
				value: 1898,
				itemStyle: createRandomItemStyle()
			},
			{
				name: "Pitch Perfect",
				value: 1484,
				itemStyle: createRandomItemStyle()
			},
			{
				name: "Express",
				value: 1112,
				itemStyle: createRandomItemStyle()
			},
			{
				name: "Home",
				value: 965,
				itemStyle: createRandomItemStyle()
			},
			{
				name: "Johnny Depp",
				value: 847,
				itemStyle: createRandomItemStyle()
			},
			{
				name: "Lena Dunham",
				value: 582,
				itemStyle: createRandomItemStyle()
			},
			{
				name: "Lewis Hamilton",
				value: 555,
				itemStyle: createRandomItemStyle()
			},
			{
				name: "KXAN",
				value: 550,
				itemStyle: createRandomItemStyle()
			},
			{
				name: "Mary Ellen Mark",
				value: 462,
				itemStyle: createRandomItemStyle()
			},
			{
				name: "Farrah Abraham",
				value: 366,
				itemStyle: createRandomItemStyle()
			},
			{
				name: "Rita Ora",
				value: 360,
				itemStyle: createRandomItemStyle()
			},
			{
				name: "Serena Williams",
				value: 282,
				itemStyle: createRandomItemStyle()
			},
			{
				name: "NCAA baseball tournament",
				value: 273,
				itemStyle: createRandomItemStyle()
			},
			{
				name: "Point Break",
				value: 265,
				itemStyle: createRandomItemStyle()
			}
		]
	}]
})

/* */
$(".timeSlider").slider({
	range: "min",
	value: 10,
	min: 1,
	max: 50,
	slide: function(event, ui) {
		$(".timeLable").html(ui.value + 'min');
	}
});

var taskChart = echarts.init(document.getElementById('taskChart'));
taskChart.setOption({
	title:{
		text:"TB49班4月8日课堂任务",
		x:'center'
	},
	tooltip: {
		formatter: "{a} <br/>{c} {b}"
	},
	toolbox: {
		show: true,
		feature: {
			//          mark : {show: true},
			//          restore : {show: true},
			saveAsImage: { show: true }
		}
	},
	series: [{
			name: '速度',
			type: 'gauge',
			z: 3,
			min: 0,
			max: 100,
			splitNumber: 10,
			axisLine: { // 坐标轴线
				lineStyle: { // 属性lineStyle控制线条样式
					width: 10
				}
			},
			axisTick: { // 坐标轴小标记
				length: 15, // 属性length控制线长
				lineStyle: { // 属性lineStyle控制线条样式
					color: 'auto'
				}
			},
			splitLine: { // 分隔线
				length: 20, // 属性length控制线长
				lineStyle: { // 属性lineStyle（详见lineStyle）控制线条样式
					color: 'auto'
				}
			},
			title: {
				textStyle: { // 其余属性默认使用全局文本样式，详见TEXTSTYLE
					fontWeight: 'bolder',
					fontSize: 20,
					fontStyle: 'italic'
				}
			},
			detail: {
				textStyle: { // 其余属性默认使用全局文本样式，详见TEXTSTYLE
					fontWeight: 'bolder'
				}
			},
			data: [{ value: 40, name: '总体完成率 (%)' }]
		},
		{
			name: '转速',
			type: 'gauge',
			center: ['25%', '55%'], // 默认全局居中
			radius: '50%',
			min: 0,
			max: 100,
			endAngle: 45,
			splitNumber: 5,
			axisLine: { // 坐标轴线
				lineStyle: { // 属性lineStyle控制线条样式
					width: 8
				}
			},
			axisTick: { // 坐标轴小标记
				length: 12, // 属性length控制线长
				lineStyle: { // 属性lineStyle控制线条样式
					color: 'auto'
				}
			},
			splitLine: { // 分隔线
				length: 20, // 属性length控制线长
				lineStyle: { // 属性lineStyle（详见lineStyle）控制线条样式
					color: 'auto'
				}
			},
			pointer: {
				width: 5
			},
			title: {
				offsetCenter: [0, '-30%'], // x, y，单位px
			},
			detail: {
				textStyle: { // 其余属性默认使用全局文本样式，详见TEXTSTYLE
					fontWeight: 'bolder'
				}
			},
			data: [{ value: 1.5, name: '提交率 (%)' }]
		},
		{
			name: 'A表',
			type: 'gauge',
			center: ['75%', '50%'], // 默认全局居中
			radius: '50%',
			min: 0,
			max: 2,
			startAngle: 135,
			endAngle: 45,
			splitNumber: 2,
			axisLine: { // 坐标轴线
				lineStyle: { // 属性lineStyle控制线条样式
					color: [
						[0.2, '#ff4500'],
						[0.8, '#48b'],
						[1, '#228b22']
					],
					width: 8
				}
			},
			axisTick: { // 坐标轴小标记
				splitNumber: 5,
				length: 10, // 属性length控制线长
				lineStyle: { // 属性lineStyle控制线条样式
					color: 'auto'
				}
			},
			axisLabel: {
				formatter: function(v) {
					switch(v + '') {
						case '0':
							return '优';
						case '1':
							return '良';
						case '2':
							return '差';
					}
				}
			},
			splitLine: { // 分隔线
				length: 15, // 属性length控制线长
				lineStyle: { // 属性lineStyle（详见lineStyle）控制线条样式
					color: 'auto'
				}
			},
			pointer: {
				width: 2
			},
			title: {
				show: false
			},
			detail: {
				show: false
			},
			data: [{ value: 0.5, name: 'XX' }]
		},
		{
			name: 'B表',
			type: 'gauge',
			center: ['75%', '50%'], // 默认全局居中
			radius: '50%',
			min: 0,
			max: 2,
			startAngle: 315,
			endAngle: 225,
			splitNumber: 2,
			axisLine: { // 坐标轴线
				lineStyle: { // 属性lineStyle控制线条样式
					color: [
						[0.2, '#ff4500'],
						[0.8, '#48b'],
						[1, '#228b22']
					],
					width: 8
				}
			},
			axisTick: { // 坐标轴小标记
				show: false
			},
			axisLabel: {
				formatter: function(v) {
					switch(v + '') {
						case '0':
							return '上';
						case '1':
							return '中';
						case '2':
							return '下';
					}
				}
			},
			splitLine: { // 分隔线
				length: 15, // 属性length控制线长
				lineStyle: { // 属性lineStyle（详见lineStyle）控制线条样式
					color: 'auto'
				}
			},
			pointer: {
				width: 2
			},
			title: {
				show: false
			},
			detail: {
				show: false
			},
			data: [{ value: 0.5, name: 'gas' }]
		}
	]
});

//clearInterval(timeTicket);
timeTicket = setInterval(function() {
	//	console.debug(taskChart._option)
	var option1 = taskChart._option
	option1.series[0].data[0].value = (Math.random() * 100).toFixed(2) - 0;
	option1.series[1].data[0].value = (Math.random() * 100).toFixed(2) - 0;
	option1.series[2].data[0].value = (Math.random() * 2).toFixed(2) - 0;
	option1.series[3].data[0].value = (Math.random() * 2).toFixed(2) - 0;
	taskChart.setOption(option1, true);
}, 2000)