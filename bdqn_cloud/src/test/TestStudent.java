package test;

import java.util.List;

import com.tb49.a.dao.StudentDao;
import com.tb49.a.dao.impl.StudentDaoImpl;
import com.tb49.a.pojo.Page;
import com.tb49.a.pojo.Student;

public class TestStudent {

	public static void main(String[] args) {
		StudentDao dao = new StudentDaoImpl();
		Page page = new Page();
		page.setIndex(1);
		page.setPageSize(30);
		List<Student> list = dao.findStudentByPage(page);
		for (Student s : list) {
			System.out.println(s);
		}
	}
}
