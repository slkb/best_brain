package com.tb49.a.dao;

import java.util.Date;
import java.util.List;

import com.tb49.a.pojo.Task;

public interface ClassTaskDao {

	List<Task> findStudentOneDayTasks(String studentId, String date);

}
