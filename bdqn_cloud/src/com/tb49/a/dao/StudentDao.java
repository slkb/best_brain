package com.tb49.a.dao;

import java.util.List;

import com.tb49.a.pojo.Page;
import com.tb49.a.pojo.Student;

public interface StudentDao {

	List<Student> findStudentByPage(Page page);
}
