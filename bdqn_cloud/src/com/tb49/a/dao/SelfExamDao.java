package com.tb49.a.dao;

import java.util.List;

import com.tb49.a.pojo.StudentExam;

public interface SelfExamDao {

	List<StudentExam> findOneDayStudentTopN(String date, int n);

}
