package com.tb49.a.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

public interface Mapper<E> {
	E doMapper(ResultSet rs) throws SQLException;
}
