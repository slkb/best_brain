package com.tb49.a.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import com.tb49.a.pojo.StudentExam;
import com.tb49.a.service.ViewService;

@SuppressWarnings("serial")
public class IndexController extends HttpServlet {

	private ViewService service = new ViewService();

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setCharacterEncoding("utf-8");
		String date = req.getParameter("date");
		List<StudentExam> list = service.findOneDayStudentTopN(date, 100);
		String json = JSON.toJSONString(list);
		resp.setContentType("application/json");
		PrintWriter pw = resp.getWriter();
		pw.print(json);
		pw.close();
	}
}
