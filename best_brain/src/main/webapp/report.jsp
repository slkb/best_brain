<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>日报</title>
<style type="text/css">
	#box{
		background-color: #eee;
		width: 900px;
		margin: 30px auto;
		padding: 30px;
	}
	table{
		border-collapse: collapse;
		width: 300px;
		margin: 20px auto;
		border-color: #ccc;
		line-height: 24px;
	}
	th{
		text-align: right;
		width: 100px;
	}
</style>
</head>
<body>
<div id="box">
	<fieldset>
		<legend>微信日报</legend>
		<table border="">
			<tr>
				<th>姓名</th>
				<td>XXX</td>
			</tr>
			<tr>
				<th>做题做题总数</th>
				<td>XXX</td>
			</tr>
			<tr>
				<th>做题正确率</th>
				<td>XXX</td>
			</tr>
			<tr>
				<th>班级排名</th>
				<td>XXX</td>
			</tr>
			<tr>
				<th>学习排名</th>
				<td>XXX</td>
			</tr>
			<tr>
				<th>班级前三名</th>
				<td>XXX</td>
			</tr>
			<tr>
				<th>年级前十名</th>
				<td>XXX</td>
			</tr>
			<tr>
				<th>提示信息</th>
				<td>XXX</td>
			</tr>
		</table>

	</fieldset>
</div>

</body>
</html>