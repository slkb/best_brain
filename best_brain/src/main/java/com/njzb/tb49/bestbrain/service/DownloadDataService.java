package com.njzb.tb49.bestbrain.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.njzb.tb49.bestbrain.dao.StudentVideoDao;
import com.njzb.tb49.bestbrain.pojo.StudentVideoOnlineTime;
import com.njzb.tb49.bestbrain.util.BdqnDataUtil;

@Service
public class DownloadDataService {
	
	private static Logger log = Logger.getLogger(DownloadDataService.class);
	
	@Resource
	private StudentVideoDao studentVideoDao;

	@Transactional
	public boolean loadLastDayQuestion(){
		Date now = new Date();
		Date lastDate = new Date(now.getTime() - 3600000*24);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String lastDateText = sdf.format(lastDate);
		log.info("准备获取["+lastDateText+"]的数据...");
		/*
		 * 1.登录
		 * 2.切换到5.0
		 * 3.获取数据
		 * 4.切换到6.0
		 * 5.获取数据
		 */
		if(BdqnDataUtil.login()){
			log.info("登录成功");
			BdqnDataUtil.loadXshSelfExamData2Db(lastDateText);
			
			List<StudentVideoOnlineTime> list = BdqnDataUtil.getVideoTime(lastDateText,1);
			for (StudentVideoOnlineTime s : list) {
				s.setOnlineDate(lastDate);
			}
			studentVideoDao.loadVideoTime(list);
			
			list = BdqnDataUtil.getVideoTime(lastDateText,2);
			for (StudentVideoOnlineTime s : list) {
				s.setOnlineDate(lastDate);
			}
			studentVideoDao.loadVideoTime(list);
			
			if(BdqnDataUtil.changeProject5()){
				list = BdqnDataUtil.getVideoTime(lastDateText,1);
				for (StudentVideoOnlineTime s : list) {
					s.setOnlineDate(lastDate);
				}
				studentVideoDao.loadVideoTime(list);
				
				list = BdqnDataUtil.getVideoTime(lastDateText,2);
				for (StudentVideoOnlineTime s : list) {
					s.setOnlineDate(lastDate);
				}
				studentVideoDao.loadVideoTime(list);
			}
			return true;
		}else{
			log.warn("登录失败");
		}
		return false;
		
	}
	
}
