package com.njzb.tb49.bestbrain.dao;

import java.util.List;

import com.njzb.tb49.bestbrain.pojo.Page;
import com.njzb.tb49.bestbrain.pojo.Student;

public interface StudentDao {

	List<Student> findStudentByPage(Page page);
}
