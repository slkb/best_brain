package com.njzb.tb49.bestbrain.pojo;

import java.util.Date;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;


/**
 * <pre>
 * 
 * {
 *       "avgTimeSpend": 0.3,
 *       "schoolName": "南京中博",
 *       "correctRate": 86,
 *       "classId": 13853,
 *       "answerQuestionCount": 437,
 *       "userName": "马具强",
 *       "realQuestionCount": 0,
 *       "className": "XSHJ5ZXDZ00000202707",
 *       "passport": "bcb0d42d1ba44253959041b5799c3ec4",
 *       "totalTimeSpend": 2.4,
 *       "boundMobile": null,
 *       "boundEmail": "514732557@qq.com"
 *     }
 * 
 * 
 * </pre>
 * 
 * @author 孟凡鼎
 *
 */
public class StudentExam {

	public StudentExam() {
	}

	public StudentExam(JSONObject json) {
		this.avgTimeSpend = json.getDouble("avgTimeSpend");
		this.correctRate = json.getInteger("correctRate");
		this.classId = json.getInteger("classId");
		this.answerQuestionCount = json.getInteger("answerQuestionCount");
		this.userName = json.getString("userName");
		this.realQuestionCount = json.getInteger("realQuestionCount");
		this.className = json.getString("className");
		this.passport = json.getString("passport");
		this.totalTimeSpend = json.getDouble("totalTimeSpend");
		// this.examDate = new Date();
		// this.boundMobile = json.getString("boundMobile");
		// this.boundEmail = json.getString("boundEmail"); // 6.0学生信息中异常
	}

	private double avgTimeSpend;
	private int correctRate;
	private int classId;
	private int answerQuestionCount;
	private String userName;
	private int realQuestionCount;
	private String className;
	private String passport;
	private double totalTimeSpend;
	private String boundMobile;
	private String boundEmail;
	private Date examDate;

	public double getAvgTimeSpend() {
		return avgTimeSpend;
	}

	public void setAvgTimeSpend(double avgTimeSpend) {
		this.avgTimeSpend = avgTimeSpend;
	}

	public int getCorrectRate() {
		return correctRate;
	}

	public void setCorrectRate(int correctRate) {
		this.correctRate = correctRate;
	}

	public int getClassId() {
		return classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public int getAnswerQuestionCount() {
		return answerQuestionCount;
	}

	public void setAnswerQuestionCount(int answerQuestionCount) {
		this.answerQuestionCount = answerQuestionCount;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getRealQuestionCount() {
		return realQuestionCount;
	}

	public void setRealQuestionCount(int realQuestionCount) {
		this.realQuestionCount = realQuestionCount;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public double getTotalTimeSpend() {
		return totalTimeSpend;
	}

	public void setTotalTimeSpend(double totalTimeSpend) {
		this.totalTimeSpend = totalTimeSpend;
	}

	public String getBoundMobile() {
		return boundMobile;
	}

	public void setBoundMobile(String boundMobile) {
		this.boundMobile = boundMobile;
	}

	public String getBoundEmail() {
		return boundEmail;
	}

	public void setBoundEmail(String boundEmail) {
		this.boundEmail = boundEmail;
	}

	public Date getExamDate() {
		return examDate;
	}

	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}
	
	@Override
	public String toString() {
		return JSON.toJSONString(this);
	}
}
