package com.njzb.tb49.bestbrain.dao;

import java.util.List;

import com.njzb.tb49.bestbrain.pojo.StudentExam;
import com.njzb.tb49.bestbrain.pojo.vo.SelfExamVO;

public interface SelfExamDao {

	List<StudentExam> findOneDayStudentTopN(String date, int n);

	List<SelfExamVO> examReport(String date);
}
