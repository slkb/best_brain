package com.njzb.tb49.bestbrain.pojo.vo;

import java.util.Date;

public class SelfExamVO {

	private int id;
	private String realName;
	private String realClass;
	private String studentNum;
	private String phone;
	private String wechat;
	private int answerQuestionCount;
	private int correctRate;
	private Date examDate;
	private int gradeRanking;
	private int classRanking;

	public int getGradeRanking() {
		return gradeRanking;
	}

	public void setGradeRanking(int gradeRanking) {
		this.gradeRanking = gradeRanking;
	}

	public int getClassRanking() {
		return classRanking;
	}

	public void setClassRanking(int classRanking) {
		this.classRanking = classRanking;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRealName() {
		return realName;
	}

	public void setRealName(String realName) {
		this.realName = realName;
	}

	public String getRealClass() {
		return realClass;
	}

	public void setRealClass(String realClass) {
		this.realClass = realClass;
	}

	public String getStudentNum() {
		return studentNum;
	}

	public void setStudentNum(String studentNum) {
		this.studentNum = studentNum;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getWechat() {
		return wechat;
	}

	public void setWechat(String wechat) {
		this.wechat = wechat;
	}

	public int getAnswerQuestionCount() {
		return answerQuestionCount;
	}

	public void setAnswerQuestionCount(int answerQuestionCount) {
		this.answerQuestionCount = answerQuestionCount;
	}

	public int getCorrectRate() {
		return correctRate;
	}

	public void setCorrectRate(int correctRate) {
		this.correctRate = correctRate;
	}

	public Date getExamDate() {
		return examDate;
	}

	public void setExamDate(Date examDate) {
		this.examDate = examDate;
	}

}
