package com.njzb.tb49.bestbrain.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.njzb.tb49.bestbrain.dao.BaseDao;
import com.njzb.tb49.bestbrain.dao.Mapper;
import com.njzb.tb49.bestbrain.dao.SelfExamDao;
import com.njzb.tb49.bestbrain.pojo.StudentExam;
import com.njzb.tb49.bestbrain.pojo.vo.SelfExamVO;

public class SelfExamDaoImpl extends BaseDao implements SelfExamDao {

	private Mapper<StudentExam> mapper = new Mapper<StudentExam>() {
		@Override
		public StudentExam doMapper(ResultSet rs) throws SQLException {
			StudentExam se = new StudentExam();
			se.setAnswerQuestionCount(rs.getInt("answerQuestionCount"));
			se.setAvgTimeSpend(rs.getInt("avgTimeSpend"));
			se.setBoundEmail(rs.getString("boundEmail"));
			se.setBoundMobile(rs.getString("boundMobile"));
			se.setClassId(rs.getInt("classId"));
			se.setClassName(rs.getString("className"));
			se.setCorrectRate(rs.getInt("correctRate"));
			se.setExamDate(rs.getDate("examDate"));
			se.setPassport(rs.getString("passport"));
			se.setRealQuestionCount(rs.getInt("realQuestionCount"));
			se.setTotalTimeSpend(rs.getInt("totalTimeSpend"));
			se.setUserName(rs.getString("userName"));
			return se;
		}
	};

	@Override
	public List<StudentExam> findOneDayStudentTopN(String date, int n) {
		String sql = "select * from exam_info where examDate = ? ORDER BY answerQuestionCount desc limit 0,?";
		return super.query(sql, mapper, date, n);
	}

	@Override
	public List<SelfExamVO> examReport(String date) {
		// TODO Auto-generated method stub
		return null;
	}

}
