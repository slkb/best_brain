package com.njzb.tb49.bestbrain.util;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.njzb.tb49.bestbrain.dao.PersistenceUtil;
import com.njzb.tb49.bestbrain.pojo.StudentExam;
import com.njzb.tb49.bestbrain.pojo.StudentVideoOnlineTime;
import com.njzb.tb49.bestbrain.util.net.NetUtil;

@SuppressWarnings("unchecked")
public class BdqnDataUtil {

	private static Logger log = Logger.getLogger(BdqnDataUtil.class);
	private static final String PASSPORT = "love@mengfanding.cn";
	private static final String PASSWORD = "262da5ab755e706508c410fd69969585";

	/***
	 * 登录北大青鸟校区管理平台
	 * 
	 * @return
	 */
	public static boolean login() {
		try {
			byte[] imgBytes = NetUtil.getBodyBytes("http://a.bdqn.cn/pb/random_code_image.jsp");
			String rand = BaiduApi.loadRand(imgBytes);
			Map<String, String> data = new HashMap<String, String>();
			data.put("passport", PASSPORT);
			data.put("userPassword", PASSWORD);
			log.debug("本次登录的验证码由百度大脑提供 ： " + rand);
			data.put("rand", rand);// 获取验证码，存在一定失败的概率
			data.put("orgId", "1");// 固定参数，必选
			Document doc = NetUtil.getHtmlDocument("http://a.bdqn.cn/pb/pbmain/page/users/user_login.action", data);
			if (doc.html().indexOf("欢迎页") != -1) {
				log.info("登录成功！");
				return true;
			}
		} catch (IOException e) {
			log.error(e);
		}

		log.info("登录失败！");
		return false;
	}

	/**
	 * 切换5.0产品
	 * 
	 * @return
	 */
	public static boolean changeProject5() {

		Map<String, String> header = new HashMap<String, String>();
		header.put("Referer", "http://a.bdqn.cn/pb/pbmain/page/users/user_login.action");
		try {
			Document doc = NetUtil.getHtmlDocument(
					"http://a.bdqn.cn/pb/pbmain/page/mainframe/frame_productChange.action?productId=10262&time=1479019706418&_=1479019633612",
					null, null, header);
			if (doc.html().indexOf("菜单") != -1) {
				return true;
			}
		} catch (IOException e) {
			log.error(e);
		}

		return false;
	}

	/**
	 * 切换6.0产品
	 * 
	 * @return
	 */
	public static boolean changeProject6() {

		Map<String, String> header = new HashMap<String, String>();
		header.put("Referer", "http://a.bdqn.cn/pb/pbmain/page/users/user_login.action");
		try {
			Document doc = NetUtil.getHtmlDocument(
					"http://a.bdqn.cn/pb/pbmain/page/mainframe/frame_productChange.action?productId=11012&time=1479019706418&_=1479019633612",
					null, null, header);
			if (doc.html().indexOf("菜单") != -1) {
				return true;
			}
		} catch (IOException e) {
			log.error(e);
		}

		return false;
	}

	/**
	 * 获取班级json信息
	 * 
	 * @return
	 */
	public static String getClassInfo() {
		Map<String, String> head = new HashMap<String, String>();
		head.put("Referer", "http://a.bdqn.cn/pb/pbsub/page/examsys/selfExamStat_list.action");
		try {
			Document doc = NetUtil.getHtmlDocument(
					"http://a.bdqn.cn/pb/pbmain/page/schoolClass/schoolClass.action?schoolId=30&classStatus=1", null,
					null, head);
			return doc.getElementsByTag("body").get(0).html().replaceAll("&quot;", "'");
		} catch (IOException e) {
			log.error(e);
		}
		return null;

	}

	/**
	 * 获取自测数据
	 * 
	 * @param start
	 * @param end
	 * @param pageSize
	 * @return
	 * 
	 */
	public static String getSelfExam(String start, String end, int pageSize) {
		if (pageSize == 0)
			pageSize = 1000;
		Map<String, String> head = new HashMap<String, String>();
		Map<String, String> data = new HashMap<String, String>();
		head.put("Referer", "http://a.bdqn.cn/pb/pbsub/page/examsys/selfExamStat_list.action");
		data.put("selectBean.schoolIds", "30");
		data.put("selectBean.classStatus", "1");
		data.put("selectBean.statType", "userDefined");
		data.put("selectBean.startDate", start);
		data.put("selectBean.endDate", end);
		data.put("page", "1");
		data.put("rows", "" + pageSize);
		try {
			Document html = NetUtil.getHtmlDocument(
					"http://a.bdqn.cn/pb/pbsub/page/examsys/selfExamStat_listJson.action", data, null, head);
			return html.getElementsByTag("body").get(0).html().replaceAll("&quot;", "'");
		} catch (IOException e) {
			log.error(e);
		}
		return null;
	}

	/**
	 * 入库
	 * 
	 * @param date
	 */
	private static void persistenceData(String date) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date d = null;
		try {
			d = sdf.parse(date);
		} catch (ParseException e) {
			log.error("格式化日期错误");
		}
		String json = BdqnDataUtil.getSelfExam(date, date, 1000);
		JSONObject jobj = JSON.parseObject(json);
		log.debug(jobj);
		JSONArray array = jobj.getJSONArray("rows");
		List<StudentExam> list = new ArrayList<StudentExam>();
		for (int i = 0; i < array.size(); i++) {
			StudentExam stu = new StudentExam(array.getJSONObject(i));
			stu.setExamDate(d);
			list.add(stu);
		}
		log.info("要存入数据库中的学生自测信息数量 ： " + list.size());
		PersistenceUtil.saveExamInfo(list);
	}

	/**
	 * 加载指定日期的学校学生信息
	 * 
	 * @param date
	 */
	public static void loadXshSelfExamData2Db(String date) {

		if (BdqnDataUtil.changeProject5())
			persistenceData(date);
		else
			log.error("切换产品5.0失败");
		if (BdqnDataUtil.changeProject6())
			persistenceData(date);
		else
			log.error("切换产品6.0失败");

	}

	private static Document getXshVideoTimeDocument200(String start, String end, int index) {
		String url = "http://a.bdqn.cn/pb/pbsub/page/study/onlineTime_list.action";
		Map<String, String> datas = new HashMap<String, String>();
		datas.put("selectBean.schoolIds", "30");
		datas.put("selectBean.productStatus", "6");
		datas.put("selectBean.classStatus", "1");
//		datas.put("selectBean.classIds", "19215");
		datas.put("startTime", start);
		datas.put("endTime", end);
		datas.put("pageNo", "" + index);
		datas.put("pageSize", "200");
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Referer", "http://a.bdqn.cn/pb/pbsub/page/study/onlineTime_list.action");
		try {
			Document doc = NetUtil.getHtmlDocument(url, datas, null, headers);
			return doc;
		} catch (IOException e) {
			log.error("", e);
		}
		return null;
	}

	public static List<StudentVideoOnlineTime> getVideoTime(String date,int index) {
		List<StudentVideoOnlineTime> list = new ArrayList<StudentVideoOnlineTime>();
		Document doc = getXshVideoTimeDocument200(date, date, index);
		Elements tbodys = doc.getElementsByTag("tbody");
		for (Element tbody : tbodys) {
			list.addAll(getTbodyTrs(tbody));
		}
		return list;
	}

	private static List<StudentVideoOnlineTime> getTbodyTrs(Element tbody) {
		List<StudentVideoOnlineTime> list = new ArrayList<StudentVideoOnlineTime>();
		Elements trs = tbody.getElementsByTag("tr");
		for (Element tr : trs) {
			list.add(convertTr2Student(tr));
		}
		if (list.size() > 0)
			return list;
		return null;
	}

	private static StudentVideoOnlineTime convertTr2Student(Element tr) {
		StudentVideoOnlineTime student = new StudentVideoOnlineTime();
		Elements tds = tr.getElementsByTag("td");
		//tds.get(0)
		student.setClassName(tds.get(0).text());
		student.setStudentNum(tds.get(1).text());
		student.setUserName(tds.get(2).text());
		student.setBoundEmail(tds.get(3).text());
		student.setBoundMobile(tds.get(4).text());
		student.setTotalTimeSpend(Double.parseDouble(tds.get(7).text()));
//		for (Element td : tds) {
//			System.out.println(td.html());
//		}
		return student;
	}
}
