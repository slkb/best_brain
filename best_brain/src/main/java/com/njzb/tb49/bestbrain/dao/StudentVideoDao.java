package com.njzb.tb49.bestbrain.dao;

import java.util.List;

import com.njzb.tb49.bestbrain.pojo.StudentVideoOnlineTime;

public interface StudentVideoDao {
	
	void loadVideoTime(List<StudentVideoOnlineTime> times);

}
