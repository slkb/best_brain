package com.njzb.tb49.bestbrain.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.njzb.tb49.bestbrain.dao.BaseDao;
import com.njzb.tb49.bestbrain.dao.ClassTaskDao;
import com.njzb.tb49.bestbrain.dao.Mapper;
import com.njzb.tb49.bestbrain.pojo.Task;

public class ClassTaskDaoImpl extends BaseDao implements ClassTaskDao {

	private Mapper<Task> mapper = new Mapper<Task>() {

		@Override
		public Task doMapper(ResultSet rs) throws SQLException {
			Task task = new Task();
			task.setClassId(rs.getString("class_id"));
			task.setCommit(rs.getBoolean("iscommit"));
			task.setId(rs.getInt("id"));
			task.setSpendTime(rs.getLong("spend_time"));
			task.setStartTime(rs.getLong("start_time"));
			task.setStudentId(rs.getString("student_id"));
			task.setTaskName(rs.getString("task_name"));
			task.setWorkDate(rs.getDate("work_date"));
			return task;
		}
	};

	@Override
	public List<Task> findStudentOneDayTasks(String studentId, String date) {
		String sql = "SELECT * FROM "
				+ "( SELECT * FROM class_task WHERE work_date = ? ) a "
				+ "LEFT JOIN ( SELECT * FROM class_task_commit WHERE student_id = ? ) b "
				+ "ON a.id = b.class_task_id";
		return super.query(sql, mapper, date, studentId);
	}

}
