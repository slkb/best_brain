package com.njzb.tb49.bestbrain.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import com.njzb.tb49.bestbrain.dao.SelfExamDao;
import com.njzb.tb49.bestbrain.dao.StudentVideoDao;
import com.njzb.tb49.bestbrain.pojo.StudentExam;
import com.njzb.tb49.bestbrain.pojo.StudentVideoOnlineTime;
import com.njzb.tb49.bestbrain.util.BdqnDataUtil;

@Service
public class ViewService {

	@Resource
	private SelfExamDao selfExamDao;
	
	@Resource
	private StudentVideoDao videoDao;

	public List<StudentExam> findOneDayStudentTopN(@Param("date") String date,@Param("n") int n) {
		return selfExamDao.findOneDayStudentTopN( date, n);
	}

	public List<StudentExam> findOneDayStudentTopN(Date date, int n) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String _date = sdf.format(date);
		return selfExamDao.findOneDayStudentTopN(_date, n);
	}
	
	public void loadVideo(Date date){
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String data_ = sdf.format(date);
		
		BdqnDataUtil.login();
		BdqnDataUtil.changeProject6();
		List<StudentVideoOnlineTime> list = BdqnDataUtil.getVideoTime(data_,1);
		for (StudentVideoOnlineTime s : list) {
			s.setOnlineDate(date);
		}
		videoDao.loadVideoTime(list);
	}

}
