package com.njzb.tb49.bestbrain.util.wechat;

import java.io.IOException;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class WechatUtil {

	private static Logger log = LoggerFactory.getLogger(WechatUtil.class);

	private static String appID = Messages.getString("wx.appid");
	private static String appsecret = Messages.getString("wx.appsecret");

	private static String token;

	private static long lastUpdateTokenTime;

	/**
	 * 更新token
	 * 
	 * @throws IOException
	 */
	public static void updateToken() throws IOException {
		Connection connection = Jsoup.connect("https://api.weixin.qq.com/cgi-bin/token");
		connection.timeout(30000);
		connection.data("grant_type", "client_credential");
		connection.data("appid", appID);
		connection.data("secret", appsecret);
		connection.ignoreContentType(true);
		String tokenbody = connection.execute().body();
		log.debug(tokenbody);
		JSONObject obj = JSON.parseObject(tokenbody);
		String token1 = obj.getString("access_token");
		log.debug(token1);
		token = token1;
		lastUpdateTokenTime = System.currentTimeMillis();
	}

	/**
	 * Token是否过期
	 * 
	 * @return
	 */
	public static boolean isTokenOutTime() {
		long time = System.currentTimeMillis() - lastUpdateTokenTime;
		if (time > 7200000) {
			return true;
		}
		return false;
	}

	public static Integer sendTemplateMessage(String message) throws IOException {
		Connection connection = Jsoup.connect("https://api.weixin.qq.com/cgi-bin/message/template/send");
		connection.timeout(30000);
		connection.ignoreContentType(true);
		connection.data("access_token", token);
		connection.requestBody(message);
		Document body = connection.post();
		log.debug(body.text());
		JSONObject obj = JSON.parseObject(body.text());
		return obj.getInteger("errcode");
	}

}
