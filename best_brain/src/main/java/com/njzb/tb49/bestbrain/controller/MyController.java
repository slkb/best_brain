package com.njzb.tb49.bestbrain.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.njzb.tb49.bestbrain.service.DownloadDataService;
import com.njzb.tb49.bestbrain.service.ReportService;

@Controller
public class MyController {
	
	private static Logger log = Logger.getLogger(MyController.class);

	@Resource
	private DownloadDataService downloadDataService;
	
	@Resource
	private ReportService reportService;
	
	@RequestMapping("/load")
	@ResponseBody
	public String home(){
		downloadDataService.loadLastDayQuestion();
		log.info("ok");
		return "ok";
	}
	@RequestMapping("/send")
	@ResponseBody
	public String send(){
		Date now = new Date();
		Date lastDate = new Date(now.getTime() - 3600000 * 24);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String lastDateText = sdf.format(lastDate);
		reportService.showReport(lastDateText);
		return "ok";
	}
	
	
	
	
	
}
