package com.njzb.tb49.bestbrain.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.njzb.tb49.bestbrain.pojo.StudentExam;

public class PersistenceUtil {
	private static Logger log = Logger.getLogger(PersistenceUtil.class);

	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			log.error(e);
		}
	}

	public static boolean saveExamInfo(List<StudentExam> list) {

		try {
			String url = "jdbc:mysql://localhost:3306/bdqn";
			String user = "root";
			String password = "root";
			Connection connection = DriverManager.getConnection(url, user, password);
			connection.setAutoCommit(false);
			String sql = "INSERT INTO `exam_info` (avgTimespend,classid,answerquestioncount,"
					+ "passport,realquestioncount,totaltimespend,boundemail,classname,boundmobile,schoolname,username,correctrate,examdate) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?)";
			long start = System.currentTimeMillis();
			PreparedStatement ps = connection.prepareStatement(sql);
			for (StudentExam se : list) {
				ps.setDouble(1, se.getAvgTimeSpend());
				ps.setInt(2, se.getClassId());
				ps.setInt(3, se.getAnswerQuestionCount());
				ps.setString(4, se.getPassport());
				ps.setInt(5, se.getRealQuestionCount());
				ps.setDouble(6, se.getTotalTimeSpend());
				ps.setString(7, se.getBoundEmail());
				ps.setString(8, se.getClassName());
				ps.setString(9, se.getBoundMobile());
				ps.setString(10, null);
				ps.setString(11, se.getUserName());
				ps.setInt(12, se.getCorrectRate());
				ps.setDate(13, new java.sql.Date(se.getExamDate().getTime()));
				ps.executeUpdate();
			}
			connection.commit();
			long end = System.currentTimeMillis();
			log.info("入库成功！ 本次用时 : " + (end - start+"ms"));
			return true;
		} catch (SQLException e) {
			log.error(e);
		}
		return false;
	}
}
