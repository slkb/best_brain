package com.njzb.tb49.bestbrain.pojo;

import java.util.Date;

public class StudentVideoOnlineTime implements Comparable<StudentVideoOnlineTime> {

	private String userName;
	private String className;
	private String passport;
	private int classId;
	private String boundEmail;
	private String boundMobile;
	private int answerQuestionCount;
	private int realQuestionCount;
	private int correctRate;
	private double avgTimeSpend;
	private double totalTimeSpend;
	private int sort;
	private String studentNum;
	private double onlineTime;
	private Date onlineDate;

	@Override
	public int compareTo(StudentVideoOnlineTime s) {
		return this.getSort() - s.getSort();
	}

	public String getStudentNum() {
		return studentNum;
	}

	public void setStudentNum(String studentNum) {
		this.studentNum = studentNum;
	}

	public double getOnlineTime() {
		return onlineTime;
	}

	public void setOnlineTime(double onlineTime) {
		this.onlineTime = onlineTime;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public int getClassId() {
		return classId;
	}

	public void setClassId(int classId) {
		this.classId = classId;
	}

	public String getBoundEmail() {
		return boundEmail;
	}

	public void setBoundEmail(String boundEmail) {
		this.boundEmail = boundEmail;
	}

	public String getBoundMobile() {
		return boundMobile;
	}

	public void setBoundMobile(String boundMobile) {
		this.boundMobile = boundMobile;
	}

	public int getAnswerQuestionCount() {
		return answerQuestionCount;
	}

	public void setAnswerQuestionCount(int answerQuestionCount) {
		this.answerQuestionCount = answerQuestionCount;
	}

	public int getRealQuestionCount() {
		return realQuestionCount;
	}

	public void setRealQuestionCount(int realQuestionCount) {
		this.realQuestionCount = realQuestionCount;
	}

	public int getCorrectRate() {
		return correctRate;
	}

	public void setCorrectRate(int correctRate) {
		this.correctRate = correctRate;
	}

	public double getAvgTimeSpend() {
		return avgTimeSpend;
	}

	public void setAvgTimeSpend(double avgTimeSpend) {
		this.avgTimeSpend = avgTimeSpend;
	}

	public double getTotalTimeSpend() {
		return totalTimeSpend;
	}

	public void setTotalTimeSpend(double totalTimeSpend) {
		this.totalTimeSpend = totalTimeSpend;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public Date getOnlineDate() {
		return onlineDate;
	}

	public void setOnlineDate(Date onlineDate) {
		this.onlineDate = onlineDate;
	}
	
	

}
