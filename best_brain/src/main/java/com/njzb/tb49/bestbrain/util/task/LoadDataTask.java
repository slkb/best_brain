package com.njzb.tb49.bestbrain.util.task;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.njzb.tb49.bestbrain.service.DownloadDataService;
import com.njzb.tb49.bestbrain.service.ReportService;

@Component
public class LoadDataTask {
	@Resource
	private DownloadDataService downloadDataService;
	@Resource
	private ReportService reportService;

	@Scheduled(cron = "0 5 8 ? * *")
	public void loadData() {
		downloadDataService.loadLastDayQuestion();
	}

	@Scheduled(cron = "0 10 8 ? * *")
	public void sendReport() {
		Date now = new Date();
		Date lastDate = new Date(now.getTime() - 3600000 * 24);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String lastDateText = sdf.format(lastDate);
		reportService.showReport(lastDateText);
	}

}
