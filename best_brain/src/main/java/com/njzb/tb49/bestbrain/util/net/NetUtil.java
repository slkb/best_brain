package com.njzb.tb49.bestbrain.util.net;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * 
 * 获取网络信息工具类
 * 
 * 
 * 2017年3月11日10:53:25
 * 
 * 
 * @author Mr.Meng
 *
 */
public class NetUtil {

	private static Logger log = Logger.getLogger(NetUtil.class);
	private static Connection connection;
	private static Map<String, String> headers;
	private static Map<String, String> cookies;

	/**
	 * 根据url获取document
	 * 
	 * @param url
	 * @param 1.参数 2.cookies 3.headers
	 * @return
	 * @throws IOException
	 */
	public static Document getHtmlDocument(String url,
			Map<String, String>... datas) throws IOException {
		connection = Jsoup.connect(url).timeout(30000);
		if (headers != null) {
			Set<String> heads = headers.keySet();
			for (String key : heads)
				connection.header(key, headers.get(key));
		}

		if (cookies != null)
			connection.cookies(cookies);
		if (datas != null && datas.length > 0) {
			if (datas[0] != null)
				connection.data(datas[0]);
			if (datas.length > 1 && datas[1] != null)
				connection.cookies(datas[1]);
			if (datas.length > 2 && datas[2] != null) {
				Set<String> heads = datas[2].keySet();
				for (String key : heads)
					connection.header(key, datas[2].get(key));
			}
		}

		Response res = connection.execute();
		headers = res.headers();
		// cookies = res.cookies();
		return res.parse();
	}

	/**
	 * 
	 * @param url
	 * @param 1.参数 2.cookies 3.headers
	 * @return
	 * @throws IOException
	 */
	public static String getHtml(String url, Map<String, String>... datas)
			throws IOException {
		return getHtmlDocument(url, datas).html();
	}

	/**
	 * 
	 * 
	 * @param url
	 * @param 1.参数 2.cookies 3.headers
	 * @return
	 * @throws IOException
	 */
	public static byte[] getBodyBytes(String url, Map<String, String>... datas)
			throws IOException {
		connection = Jsoup.connect(url).timeout(30000).ignoreContentType(true);
		if (headers != null) {
			Set<String> heads = headers.keySet();
			for (String key : heads)
				connection.header(key, headers.get(key));
		}

		if (cookies != null)
			connection.cookies(cookies);

		if (datas != null && datas.length > 0) {
			if (datas[0] != null)
				connection.data(datas[0]);
			if (datas[1] != null && datas[1] != null)
				connection.cookies(datas[1]);
			if (datas[2] != null && datas[2] != null) {
				Set<String> heads = datas[2].keySet();
				for (String key : heads)
					connection.header(key, datas[2].get(key));
			}
		}

		Response res = connection.execute();
		headers = res.headers();
		cookies = res.cookies(); // 只有在获取图片时才会得到两个必须的参数
		log.debug("本次cookies是："+cookies);
		return res.bodyAsBytes();
	}
}
