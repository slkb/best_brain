package com.njzb.tb49.bestbrain.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class BaseDao {
	private static Logger log = Logger.getLogger(BaseDao.class);

	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			log.error("", e);
		}

	}

	private static String url = "jdbc:mysql://localhost:3306/bdqn";
	private static String user = "root";
	private static String password = "root";
	private Connection con;
	private PreparedStatement ps;
	private ResultSet rs;

	public Connection openConnection() {
		try {
			con = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			log.error("", e);
		}
		return con;
	}

	public PreparedStatement getPreparedStatement(String sql) {
		try {
			ps = openConnection().prepareStatement(sql);
		} catch (SQLException e) {
			log.error("", e);
		}
		return ps;
	}

	public <E> int batchUpdate(String sql, Object... params) {
		return 0;
	}

	public <E> List<E> query(String sql, Mapper<E> mapper, Object... params) {
		try {
			ps = getPreparedStatement(sql);
			if (params != null && params.length > 0)
				for (int i = 0; i < params.length; i++) {
					ps.setObject(i + 1, params[i]);
				}
			rs = ps.executeQuery();
			List<E> list = new ArrayList<E>();
			while (rs.next())
				list.add(mapper.doMapper(rs));
			if (list.size() > 0)
				return list;
		} catch (SQLException e) {
			log.error("", e);
		}
		return null;
	}

	public void close() {
		try {
			if (rs != null) {
				rs.close();
				rs = null;
			}
			if (ps != null) {
				ps.close();
				ps = null;
			}
			if (con != null) {
				con.close();
				con = null;
			}
		} catch (SQLException e) {
			log.error("", e);
		}
	}
}
