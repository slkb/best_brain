package com.njzb.tb49.bestbrain.util.wechat.pojo;

public class Font {

	private String value;
	private String color;

	public Font() {
		super();
	}

	public Font(String value) {
		super();
		this.value = value;
	}

	public Font(String value, String color) {
		super();
		this.value = value;
		this.color = color;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getColor() {
		if (color == null)
			color = "#000";
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
