package com.njzb.tb49.bestbrain.test;

import java.util.List;

import com.alibaba.fastjson.JSON;
import com.njzb.tb49.bestbrain.pojo.StudentVideoOnlineTime;
import com.njzb.tb49.bestbrain.util.BdqnDataUtil;

public class TestD {

	public static void main(String[] args) {
		BdqnDataUtil.login();
		BdqnDataUtil.changeProject6();
		List<StudentVideoOnlineTime> list = BdqnDataUtil.getVideoTime("2017-06-01",1);
		for (StudentVideoOnlineTime s : list) {
			System.out.println(JSON.toJSONString(s));
		}

	}
}
