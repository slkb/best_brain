package com.njzb.tb49.bestbrain.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSONObject;
import com.njzb.tb49.bestbrain.dao.SelfExamDao;
import com.njzb.tb49.bestbrain.pojo.vo.SelfExamVO;
import com.njzb.tb49.bestbrain.util.wechat.WechatUtil;
import com.njzb.tb49.bestbrain.util.wechat.pojo.Font;

@Service
public class ReportService {
	
	@Autowired
	private SelfExamDao selfExamDao;
	
	
	public void showReport(String date){
		List<SelfExamVO> vos = selfExamDao.examReport(date);
		Map<String, List<SelfExamVO>> classList = new HashMap<String,List<SelfExamVO>>();
		
		List<String> gradeTop10 = new ArrayList<String>();
		
		
		for(int i =0;i<vos.size();i++){
			SelfExamVO selfExamVO = vos.get(i);
			if(i<10){
				gradeTop10.add(selfExamVO.getRealName());
			}
			selfExamVO.setGradeRanking(i+1);
			String className = selfExamVO.getRealClass();
			if(classList.containsKey(className)){
				List<SelfExamVO> classInfos = classList.get(className);
				selfExamVO.setClassRanking(classInfos.size()+1);
				classInfos.add(selfExamVO);
			}else{
				List<SelfExamVO> list = new ArrayList<SelfExamVO>();
				classList.put(className, list);
				selfExamVO.setClassRanking(1);
				classList.get(className).add(selfExamVO);
			}
		}
		
		String gradeTop10str = "";
		for (int i =0;i<gradeTop10.size();i++) {
			gradeTop10str+=(i+1 +"."+gradeTop10.get(i)+" ");
		}
		
		Set<String> keys = classList.keySet();
		try {
			WechatUtil.updateToken();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		
		for (String className : keys) {
			List<SelfExamVO> list = classList.get(className);
			String classTop3 = "";
			for(int i = 0;i<list.size();i++){
				if(i>2)
					break;
				classTop3+=(i+1+"."+list.get(i).getRealName()+" ");
			}
			for (SelfExamVO s : list) {
				if(s.getWechat()==null || s.getWechat().length()<1)
					continue;
				JSONObject msg = new JSONObject();
				JSONObject data = new JSONObject();
				
				data.put("name", new Font(s.getRealName()));
				data.put("count", new Font(s.getAnswerQuestionCount()+""));
				data.put("rate", new Font(s.getCorrectRate()+""));
				data.put("classTop", new Font(s.getClassRanking()+""));
				data.put("gradeTop", new Font(s.getGradeRanking()+""));
				data.put("gradeTop10", new Font(gradeTop10str));
				data.put("classTop3", new Font(classTop3));
				if(s.getAnswerQuestionCount() > 100){
					data.put("msg", new Font("表现不错，今天的任务完成了","#00f"));
				}else{
					data.put("msg", new Font("搞事情，今天的任务都没完成","#f00"));
				}
				msg.put("data", data);
				msg.put("template_id", "pgzvNA8BqNk3sQoKfumyMhYIX5pzOF16a7oUVedX-EY");
				msg.put("touser", s.getWechat());
				try {
					WechatUtil.sendTemplateMessage(msg.toJSONString());
				} catch (IOException e) {
					e.printStackTrace();
				}
//				System.out.println(s.getRealClass()+"\t\t\t"+s.getRealName()+"\t"+s.getAnswerQuestionCount()+"\t"+s.getGradeRanking()+"\t"+s.getClassRanking());
			}
			
		}
		
		
	}
	
	

}
