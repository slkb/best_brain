package com.njzb.tb49.bestbrain.dao;

import java.util.List;

import com.njzb.tb49.bestbrain.pojo.Task;

public interface ClassTaskDao {

	List<Task> findStudentOneDayTasks(String studentId, String date);

}
