package com.njzb.tb49.bestbrain.pojo;

public class Page {

	private Integer index;
	private Integer pageSize;
	private int start;

	public Integer getIndex() {
		if (index == null)
			index = 1;
		return index;
	}

	public void setIndex(Integer index) {
		this.index = index;
	}

	public Integer getPageSize() {
		if (pageSize == null)
			pageSize = 10;
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public int getStart() {
		start = (getIndex() - 1) * getPageSize();
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

}
