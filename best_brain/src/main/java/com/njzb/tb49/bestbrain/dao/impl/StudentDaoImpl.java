package com.njzb.tb49.bestbrain.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.njzb.tb49.bestbrain.dao.BaseDao;
import com.njzb.tb49.bestbrain.dao.Mapper;
import com.njzb.tb49.bestbrain.dao.StudentDao;
import com.njzb.tb49.bestbrain.pojo.Page;
import com.njzb.tb49.bestbrain.pojo.Student;

public class StudentDaoImpl extends BaseDao implements StudentDao {

	private Mapper<Student> studentMapper = new Mapper<Student>() {

		@Override
		public Student doMapper(ResultSet rs) throws SQLException {
			Student s = new Student();
			s.setClassName(rs.getString("className"));
			s.setPhone(rs.getString("phone"));
			s.setProduct(rs.getString("product"));
			s.setRegisterDate(rs.getDate("registerDate"));
			s.setStatus(rs.getInt("status"));
			s.setStudentId(rs.getString("studentId"));
			s.setStudentName(rs.getString("studentName"));
			s.setStudentPassport(rs.getString("studentPassport"));
			s.setValidity(rs.getDate("validity"));
			return s;
		}
	};

	@Override
	public List<Student> findStudentByPage(Page page) {
		String sql = "select * from student order by className,studentName limit ?,?";
		return super.query(sql, studentMapper, page.getStart(),
				page.getPageSize());
	}

}
