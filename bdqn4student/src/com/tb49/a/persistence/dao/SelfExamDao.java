package com.tb49.a.persistence.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.tb49.a.pojo.StudentExam;

public interface SelfExamDao {

	List<StudentExam> findOneDayStudentTopN(@Param("date") String date, @Param("n") int n);

}
