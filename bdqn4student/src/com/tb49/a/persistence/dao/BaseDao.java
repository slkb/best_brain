package com.tb49.a.persistence.dao;

import java.io.IOException;
import java.io.Reader;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.log4j.Logger;

public class BaseDao {
	
	private static Logger log = Logger.getLogger(BaseDao.class);
	
	private static SqlSessionFactoryBuilder sqlSessionFactoryBuilder;
	private static Reader reader;
	static{
		try {
			String resource = "mybatis-config.xml";
			reader = Resources.getResourceAsReader(resource);
			sqlSessionFactoryBuilder = new SqlSessionFactoryBuilder();
		} catch (IOException e) {
			log.error("",e);
		}
	}
	
	public SqlSession openSession(){
		SqlSessionFactory sqlSessionFactory = sqlSessionFactoryBuilder.build(reader);
		return sqlSessionFactory.openSession();
	}
}
