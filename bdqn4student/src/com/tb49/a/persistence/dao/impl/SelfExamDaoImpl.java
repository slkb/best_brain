package com.tb49.a.persistence.dao.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.tb49.a.persistence.dao.BaseDao;
import com.tb49.a.persistence.dao.SelfExamDao;
import com.tb49.a.pojo.StudentExam;

public class SelfExamDaoImpl extends BaseDao implements SelfExamDao {

	@Override
	public List<StudentExam> findOneDayStudentTopN(String date, int n) {
		SqlSession session = openSession();
		SelfExamDao dao = session.getMapper(SelfExamDao.class);
		List<StudentExam> list = dao.findOneDayStudentTopN(date, n);
		session.close();
		return list;
	}

}
