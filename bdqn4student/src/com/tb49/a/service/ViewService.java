package com.tb49.a.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.tb49.a.persistence.dao.SelfExamDao;
import com.tb49.a.persistence.dao.impl.SelfExamDaoImpl;
import com.tb49.a.pojo.StudentExam;

public class ViewService {

	private SelfExamDao dao = new SelfExamDaoImpl();

	public List<StudentExam> findOneDayStudentTopN(String date, int n) {
		return dao.findOneDayStudentTopN(date, n);
	}

	public List<StudentExam> findOneDayStudentTopN(Date date, int n) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String _date = sdf.format(date);
		return dao.findOneDayStudentTopN(_date, n);
	}

}
