package test;

import java.util.List;

import com.tb49.a.persistence.dao.SelfExamDao;
import com.tb49.a.persistence.dao.impl.SelfExamDaoImpl;
import com.tb49.a.pojo.StudentExam;

public class TestDayStudentExamInfo {

	public static void main(String[] args) {
		SelfExamDao dao = new SelfExamDaoImpl();
		List<StudentExam> list = dao.findOneDayStudentTopN("2017-3-27",30);
		for (StudentExam se : list) {
			System.out.println(se);
		}
	}
}
