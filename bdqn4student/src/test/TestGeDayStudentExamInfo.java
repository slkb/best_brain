package test;

import org.apache.log4j.Logger;

import com.tb49.a.service.BdqnDataUtil;

public class TestGeDayStudentExamInfo {

	static Logger log = Logger.getLogger(TestGeDayStudentExamInfo.class);

	public static void main(String[] args) {
		String date = "2017-3-21";
		if(BdqnDataUtil.login())
			BdqnDataUtil.loadXshSelfExamData2Db(date);
		log.info("done !");
	}
}
