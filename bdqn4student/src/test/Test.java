package test;

import java.io.IOException;
import java.util.Iterator;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.tb49.a.net.NetUtil;


public class Test {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException {
		
		Document doc = NetUtil.getHtmlDocument("https://movie.douban.com/tag/%E7%88%B1%E6%83%85?start=20&type=T");
		Elements els = doc.getElementsByClass("pl2");
		Iterator<Element> it = els.iterator();
		while(it.hasNext()){
			Element e = it.next();
			Elements as = e.getElementsByTag("a");			
			Element a = as.get(0);
			System.out.println(a.html());
			Elements _span = a.getElementsByTag("span");
			if(_span!=null)
				System.out.println(_span.get(0).html());
			
		}
		
		
	}

}
