/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : mengfanding

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2017-03-28 18:44:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for exam_info
-- ----------------------------
DROP TABLE IF EXISTS `exam_info`;
CREATE TABLE `exam_info` (
  `avgTimeSpend` double(11,1) DEFAULT NULL,
  `classId` int(11) DEFAULT NULL,
  `answerQuestionCount` int(11) DEFAULT NULL,
  `passport` varchar(255) DEFAULT NULL,
  `realQuestionCount` int(11) DEFAULT NULL,
  `totalTimeSpend` double(11,1) DEFAULT NULL,
  `boundEmail` varchar(255) DEFAULT NULL,
  `className` varchar(255) DEFAULT NULL,
  `boundMobile` varchar(255) DEFAULT NULL,
  `schoolName` varchar(255) DEFAULT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `correctRate` int(11) DEFAULT NULL,
  `examDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
