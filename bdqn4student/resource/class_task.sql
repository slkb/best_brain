/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50547
Source Host           : localhost:3306
Source Database       : bdqn

Target Server Type    : MYSQL
Target Server Version : 50547
File Encoding         : 65001

Date: 2017-04-23 20:22:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for class_task
-- ----------------------------
DROP TABLE IF EXISTS `class_task`;
CREATE TABLE `class_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_name` varchar(255) DEFAULT NULL,
  `start_time` bigint(20) DEFAULT NULL,
  `spend_time` bigint(20) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `work_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of class_task
-- ----------------------------
INSERT INTO `class_task` VALUES ('1', '课堂练习1', '2017324234', '600000', '1', '2017-04-23');
INSERT INTO `class_task` VALUES ('2', '课堂练习2', '201723423423', '600000', '1', '2017-04-23');

-- ----------------------------
-- Table structure for class_task_commit
-- ----------------------------
DROP TABLE IF EXISTS `class_task_commit`;
CREATE TABLE `class_task_commit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `class_task_id` int(11) DEFAULT NULL,
  `student_id` varchar(11) DEFAULT NULL,
  `isCommit` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of class_task_commit
-- ----------------------------
INSERT INTO `class_task_commit` VALUES ('1', '1', '1', '1');
INSERT INTO `class_task_commit` VALUES ('2', '1', '2', '1');
