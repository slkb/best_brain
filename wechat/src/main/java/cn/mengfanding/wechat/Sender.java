package cn.mengfanding.wechat;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;

import cn.mengfanding.pojo.MessageBody;
import cn.mengfanding.pojo.ResponseBody;

public class Sender {

	private static String access_token;
	private static int errorCount;
	private static Long lastTokenTime;

	public static boolean reloadToken() {
		Map<String, String> datas = new HashMap<String, String>();
		datas.put("grant_type", "client_credential");
		datas.put("appid", "wx792468eaa31e439f");
		datas.put("secret", "38a3686206dc10ba74b364d9d6232fa3");
		try {
			String result = NetUtil.get("https://api.weixin.qq.com/cgi-bin/token", datas, null);
			ResponseBody resp = JSON.parseObject(result, ResponseBody.class);
			if (resp.getErrcode() == 0) {
				access_token = resp.getAccess_token();
				errorCount = 0;
				Date date = new Date();
				lastTokenTime = date.getTime();
				return true;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		errorCount++;
		if (errorCount < 3) {
			reloadToken();
		}
		return false;
	}

	public static String getToken() {
		if (!isAlive())
			reloadToken();
		return access_token;
	}

	private static boolean isAlive() {
		if (lastTokenTime == null)
			return false;
		Date date = new Date();
		long now = date.getTime();
		if (lastTokenTime + 7200000 <= now)
			return false;
		if (access_token == null)
			return false;
		return true;

	}

	public static ResponseBody sendMessage(String url, Map<String, String> datas, MessageBody msg) {
		String requestBody = JSON.toJSONString(msg);
		try {
			String result = NetUtil.post(url, datas, requestBody);
			ResponseBody resp = JSON.parseObject(result, ResponseBody.class);
			return resp;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

}
