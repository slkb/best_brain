package cn.mengfanding.wechat;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class NetUtil {

	public static Connection send(String url, Map<String, String> datas, String requestBody) {
		Connection conn = Jsoup.connect(url);
		if (datas != null) {
			Set<String> keys = datas.keySet();
			for (String key : keys) {
				conn.data(key, datas.get(key));
			}
		}
		if (requestBody != null)
			conn.requestBody(requestBody);
		conn.ignoreContentType(true);
		return conn;
	}

	public static String get(String url, Map<String, String> datas, String requestBody) throws IOException {
		Connection connection = send(url, datas, requestBody);
		return connection.execute().body();
	}

	public static String post(String url, Map<String, String> datas, String requestBody) throws IOException {
		Connection connection = send(url, datas, requestBody);
		Document doc = connection.post();
		return doc.text();
	}

}
