package cn.mengfanding.pojo;

public class TempDailyReport {

	private Value name;
	private Value count;
	private Value classTop;
	private Value gradeTop;
	private Value msg;
	private Value rate;
	private Value classTop3;
	private Value gradeTop10;

	public TempDailyReport() {
		super();
	}

	public TempDailyReport(Value name, Value count, Value classTop, Value gradTop, Value msg) {
		super();
		this.name = name;
		this.count = count;
		this.classTop = classTop;
		this.gradeTop = gradTop;
		this.msg = msg;
	}

	public TempDailyReport(String string, String string2, String string3, String string4, String string5, String rate,
			String calssTop3, String gradeTop10) {
		this.name = new Value(string, "#666");
		this.count = new Value(string2, "#f00");
		this.classTop = new Value(string3, "#f00");
		this.gradeTop = new Value(string4, "#f00");
		this.msg = new Value(string5, "#f00");
		this.rate = new Value(rate, "#f00");
		this.classTop3 = new Value(calssTop3, "#999");
		this.gradeTop10 = new Value(gradeTop10, "#999");

	}

	public Value getName() {
		return name;
	}

	public void setName(Value name) {
		this.name = name;
	}

	public Value getCount() {
		return count;
	}

	public void setCount(Value count) {
		this.count = count;
	}

	public Value getClassTop() {
		return classTop;
	}

	public void setClassTop(Value classTop) {
		this.classTop = classTop;
	}

	public Value getGradeTop() {
		return gradeTop;
	}

	public void setGradeTop(Value gradeTop) {
		this.gradeTop = gradeTop;
	}

	public Value getMsg() {
		return msg;
	}

	public void setMsg(Value msg) {
		this.msg = msg;
	}

	public Value getRate() {
		return rate;
	}

	public void setRate(Value rate) {
		this.rate = rate;
	}

	public Value getClassTop3() {
		return classTop3;
	}

	public void setClassTop3(Value classTop3) {
		this.classTop3 = classTop3;
	}

	public Value getGradeTop10() {
		return gradeTop10;
	}

	public void setGradeTop10(Value gradeTop10) {
		this.gradeTop10 = gradeTop10;
	}

}
