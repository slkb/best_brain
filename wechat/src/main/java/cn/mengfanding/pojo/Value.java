package cn.mengfanding.pojo;

public class Value {

	private String value;
	private String color;

	public Value(String value) {
		super();
		this.value = value;
	}

	public Value(String value, String color) {
		super();
		this.value = value;
		this.color = color;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getColor() {
		if (color == null)
			color = "#000";
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}
