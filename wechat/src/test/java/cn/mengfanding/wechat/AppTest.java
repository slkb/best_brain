package cn.mengfanding.wechat;

import java.util.HashMap;
import java.util.Map;

import cn.mengfanding.pojo.MessageBody;
import cn.mengfanding.pojo.ResponseBody;
import cn.mengfanding.pojo.TempDailyReport;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Unit test for simple App.
 */
public class AppTest extends TestCase {
	/**
	 * Create the test case
	 *
	 * @param testName
	 *            name of the test case
	 */
	public AppTest(String testName) {
		super(testName);
	}

	/**
	 * @return the suite of tests being tested
	 */
	public static Test suite() {
		return new TestSuite(AppTest.class);
	}

	/**
	 * Rigourous Test :-)
	 */
	public void testApp() {
		String url = "https://api.weixin.qq.com/cgi-bin/message/template/send";
		MessageBody msg = new MessageBody();
		msg.setTemplate_id("AWLekw62jtynZvCe4bQajSDaxlVRdt6Pabbut9KL7XQ");
		msg.setUrl("http://www.mengfanding.cn");
		msg.setTouser("oxW2WwpySjLSeqs7UANccwLK0dgs");
//		TempDailyReport data = new TempDailyReport("孟先生", "1", "2", "3", "昨天校排行榜：张三 李四 王地五 是是否 张三 李四 王地五 是是否 ；  昨天班排行榜：TB23 TB26 TB39 TB49 TB108 ;   你还有什么好说的吗？");

//		msg.setData(data);
		Map<String, String> token = new HashMap<String, String>();
		token.put("access_token", Sender.getToken());
		ResponseBody body = Sender.sendMessage(url, token , msg);
		System.out.println(body.getErrmsg());
		assertTrue(body.getErrcode() == 0);
	}
}
